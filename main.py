from typing import List
import argparse


def syracuse_sequence(n: int) -> List[int]:
    result = [n]
    while n != 1:
        if n % 2 == 0:
            n //= 2
        else:
            n = 3 * n + 1
        result.append(n)
    return result


def syracuse_max(n: int) -> int:
    """
    Выводит максимальный элемент
    :param n:
    :return:
    """
    return sorted(syracuse_sequence(n))[-1]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("command", type=str, choices=["sequence", "max"])
    parser.add_argument("n", type=int)
    args = parser.parse_args()
    if args.n <= 0:
        print("Ошибка ввода! Число n должно быть больше нуля")
        return
    if args.command == "sequence":
        print(syracuse_sequence(args.n))
    else:
        print(syracuse_max(args.n))


if __name__ == "__main__":
    main()
